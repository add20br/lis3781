
DROP TABLE IF EXISTS plan, dependant, employee, job, emp_history, benefit;
CREATE TABLE plan
(
pln_id SMALLINT NOT NULL AUTO_INCREMENT,
pln_name VARCHAR(45) NOT NULL,
pln_type VARCHAR(10) NOT NULL check (pln_type in('single','spouse','family')),
pln_cost INT NOT NULL check (pln_cost > 0 and pln_cost <= 500),
pln_notes VARCHAR(255) NULL,
PRIMARY KEY (pln_id)
);

CREATE TABLE benefit
(
ben_id TINYINT NOT NULL AUTO_INCREMENT,
ben_name VARCHAR(45) NOT NULL,
ben_notes VARCHAR(255),
PRIMARY KEY (ben_id)
);

CREATE TABLE job
(
job_id SMALLINT NOT NULL AUTO_INCREMENT,
job_title VARCHAR(45) NOT NULL check (job_title in ('janitor', 'IT', 'Secretary', 'Cashier', 'Service Tech', 'Manager')),
job_descript VARCHAR(100) NOT NULL,
job_notes VARCHAR(255) NULL,
PRIMARY KEY (job_id)
);



CREATE TABLE employee
(
emp_id SMALLINT NOT NULL AUTO_INCREMENT, 
emp_fname VARCHAR(15) NOT NULL,
emp_lname VARCHAR(45) NOT NULL,
emp_ssn INT NOT NULL check (emp_ssn > 0 and emp_ssn <= 99999999999),
emp_dob DATE NOT NULL,
emp_street VARCHAR(45) NOT NULL,
emp_city VARCHAR(30) NOT NULL,
emp_state CHAR(2) NOT NULL DEFAULT 'FL',
emp_zip CHAR(9) NOT NULL check (emp_zip >= 10000 and emp_zip <= 99999),
emp_email VARCHAR(100) NOT NULL,
emp_phone BIGINT NOT NULL,
emp_start DATE NOT NULL,
emp_end DATE NULL,
emp_salary INT NOT NULL check (emp_salary >= 15000 and emp_salary <= 500000),
emp_notes VARCHAR(255) NULL,
PRIMARY KEY (emp_id) 
);

CREATE TABLE dependant
(
dep_id SMALLINT NOT NULL AUTO_INCREMENT,
emp_id SMALLINT NOT NULL,
pln_id SMALLINT NOT NULL,
dep_fname VARCHAR(15) NOT NULL,
dep_lname VARCHAR(45) NOT NULL,
dep_type CHAR(1) NOT NULL check (dep_type in('f','m','s','c')),
dep_ssn INT NOT NULL check (dep_ssn > 0 and dep_ssn <= 99999999999),
dep_street VARCHAR(100) NOT NULL, 
dep_city VARCHAR(30) NOT NULL,
dep_state CHAR(2) NOT NULL DEFAULT 'FL',
dep_zip CHAR(9) NOT NULL check (dep_zip >= 10000 and dep_zip <= 99999), 
dep_email VARCHAR(100) NOT NULL,
dep_phone BIGINT NOT NULL,
dep_date_added DATE NOT NULL,
dep_notes VARCHAR(255) NOT NULL,
PRIMARY KEY (dep_id),

CONSTRAINT fk_dep_plan 
FOREIGN KEY (pln_id) 
REFERENCES plan (pln_id)
ON DELETE CASCADE
ON UPDATE CASCADE,

CONSTRAINT fk_dep_emp FOREIGN KEY (emp_id)
REFERENCES employee (emp_id)
ON DELETE CASCADE
ON UPDATE CASCADE
);

CREATE TABLE emp_history LIKE employee;
ALTER TABLE emp_history MODIFY COLUMN emp_id int NOT NULL,
DROP PRIMARY KEY, ENGINE=MYISAM, ADD action VARCHAR(8) DEFAULT 'insert' FIRST,
ADD revision INT NOT NULL AUTO_INCREMENT AFTER Action,
ADD dt_datetime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER revision,
ADD PRIMARY KEY (emp_id, revision);


INSERT INTO `employee` (`emp_id`,`emp_fname`,`emp_lname`,`emp_ssn`,`emp_dob`,`emp_street`,`emp_city`,`emp_state`,`emp_zip`,`emp_email`,`emp_phone`,`emp_start`,`emp_end`,`emp_salary`,`emp_notes`)
VALUES
  (1,"Althea","Montgomery","446156277","1967-06-28","P.O. Box 508, 7674 Dolor St.","North Las Vegas","MA","38016","m-althea651@yahoo.couk","7585181333","2013-01-01 ","2022-08-23","79000","amet luctus vulputate, nisi sem semper erat,"),
  (2,"Oleg","Rowe","166131473","1974-10-23","P.O. Box 731, 4642 Mauris, St.","Reading","MT","30266","oleg_rowe@icloud.couk","9523238782","2018-11-26 ","2021-10-03","71000","lectus rutrum urna, nec luctus felis purus"),
  (3,"Hyatt","Robles","585093363","1998-09-26","Ap #480-8128 Luctus Rd.","Jackson","VA","33935","hyatt-robles1981@aol.org","7936638484","2016-05-10 ","2021-12-06","91000","Phasellus vitae mauris sit amet lorem semper auctor."),
  (4,"Keaton","Powell","861685450","1953-08-08","304-1761 Egestas St.","Sioux City","MA","41465","p-keaton@hotmail.net","1493307069","2008-02-03 ","2022-10-04","45000","molestie tellus. Aenean egestas hendrerit neque."),
  (5,"Colorado","Salinas","367113223","1978-03-17","Ap #964-6085 Magna Street","San Jose","OK","32250","csalinas@icloud.edu","5777215043","2012-09-14 ","2021-07-02","51000","fames ac turpis");

INSERT INTO `dependant` (`dep_id`,`dep_fname`,`dep_lname`,`dep_ssn`,`dep_street`,`dep_city`,`dep_state`,`dep_zip`,`dep_email`,`dep_phone`,`dep_date_added`,`dep_notes`)
VALUES
  (1,"Amaya","Woods","517425990","Ap #142-7744 Donec Street","Kansas City","AR","47883","amaya.woods@yahoo.ca","5262582861","2020-10-01 ","egestas hendrerit neque. In ornare sagittis felis. Donec tempor,"),
  (2,"Suki","Stark","376782593","Ap #990-4337 Magnis Ave","West Valley City","FL","72513","stark-suki5267@google.edu","2452581541","2008-06-29 ","nulla ante,"),
  (3,"Calista","Lowe","651440166","3058 Odio Avenue","South Bend","ID","16942","lowecalista@google.net","2695702611","2012-02-02 ","lobortis quis, pede. Suspendisse dui. Fusce diam nunc, ullamcorper"),
  (4,"Garth","Moreno","742452755","Ap #967-7553 Arcu. Rd.","Evansville","CT","79716","moreno.garth4509@yahoo.com","4422947172","2016-09-23 ","aliquet lobortis, nisi nibh lacinia"),
  (5,"Helen","Alston","637352143","525-9683 Tellus. Rd.","Paradise","AZ","75344","h_alston@yahoo.org","7947312618","2017-05-19 ","Nunc ullamcorper,");
