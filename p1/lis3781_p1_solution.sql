DROP SCHEMA IF EXISTS add20br;
CREATE SCHEMA IF NOT EXISTS add20br;
USE add20br;

/*FOR CREATING PERSON TABLE*/
DROP TABLE IF EXISTS person;
CREATE TABLE IF NOT EXISTS person (
per_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
per_ssn BINARY(64) NULL,
per_delta BINARY(64) NULL,
per_fname VARCHAR(15) NOT NULL,
per_lname VARCHAR(30) NOT NULL,
per_street VARCHAR(30) NOT NULL,
per_city VARCHAR(30) NOT NULL,
per_state CHAR(2) NOT NULL,
per_zip CHAR(9) NOT NULL,
per_email VARCHAR(100) NOT NULL,
per_dob DATE NOT NULL,
per_type ENUM('a','c','j') NOT NULL,
per_notes VARCHAR(255) NULL,
PRIMARY KEY (per_id),
UNIQUE INDEX ux_per_ssn (per_ssn ASC)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;


/*FOR CREATING CLIENT TABLE*/
DROP TABLE IF EXISTS client;
CREATE TABLE IF NOT EXISTS client (
    per_id SMALLINT UNSIGNED NOT NULL,
    cli_notes VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (per_id),

    INDEX idx_per_id (per_id ASC),
    CONSTRAINT fk_client_person
    FOREIGN KEY (per_id)
    REFERENCES person(per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

/*FOR CREATING ATTORNEY TABLE*/
DROP TABLE IF EXISTS attorney;
CREATE TABLE IF NOT EXISTS attorney (
    per_id SMALLINT UNSIGNED NOT NULL,
    aty_start_date DATE NOT NULL,
    aty_end_date DATE NULL,
    aty_hourly_rate DECIMAL(5,2) NOT NULL,
    aty_years_in_practice TINYINT NOT NULL,
    aty_notes VARCHAR(255) NULL,
    PRIMARY KEY (per_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_attorney_person 
    FOREIGN KEY (per_id) 
    REFERENCES person (per_id) 
    ON DELETE NO ACTION 
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

/*FOR CREATING COURT TABLE*/
DROP TABLE IF EXISTS court;
CREATE TABLE IF NOT EXISTS court (
    crt_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    crt_name VARCHAR(45) NOT NULL,
    crt_street VARCHAR(30) NOT NULL,
    crt_city VARCHAR(30) NOT NULL,
    crt_state CHAR(2) NOT NULL,
    crt_zip CHAR(9) NOT NULL,
    crt_phone BIGINT NOT NULL,
    crt_email VARCHAR(100) NOT NULL,
    crt_url VARCHAR(100) NOT NULL,
    crt_notes VARCHAR(255) NULL,
    PRIMARY KEY (crt_id)
)

ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


/*FOR CREATING JUDGE TABLE*/
DROP TABLE IF EXISTS judge;
CREATE TABLE IF NOT EXISTS judge (
    per_id SMALLINT UNSIGNED NOT NULL,
    crt_id TINYINT UNSIGNED NULL DEFAULT NULL,
    jud_salary DECIMAL(8,2) NOT NULL,
    jud_years_in_practice TINYINT UNSIGNED NOT NULL,
    jud_notes VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (per_id),

    INDEX idx_per_id (per_id ASC),
    INDEX idx_crt_id (crt_id ASC),

CONSTRAINT fk_judge_person
FOREIGN KEY (per_id)
REFERENCES person (per_id)
ON DELETE NO ACTION
ON UPDATE CASCADE,

CONSTRAINT fk_judge_court
    FOREIGN KEY (crt_id)
    REFERENCES court (crt_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
);

/*FOR CREATING JUDGE HISTORY TABLE*/
DROP TABLE IF EXISTS judge_hist;
CREATE TABLE IF NOT EXISTS judge_hist (
    jhs_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    jhs_crt_id TINYINT NULL,
    jhs_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    jhs_type ENUM('i','u','d') NOT NULL DEFAULT 'i',
    jhs_salary DECIMAL(8,2) NOT NULL,
    jhs_notes VARCHAR(255) NULL,
    PRIMARY KEY (jhs_id),

    INDEX idx_per_id(per_id ASC),
    CONSTRAINT fk_judge_hist_judge
        FOREIGN KEY (per_id)
        REFERENCES judge (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
);
/*FOR CREATING BAR TABLE*/
DROP TABLE IF EXISTS bar;
CREATE TABLE IF NOT EXISTS bar (
    bar_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    bar_name VARCHAR(45) NOT NULL,
    bar_notes VARCHAR(255) NULL,
    PRIMARY KEY (BAR_id),
    INDEX idx_per_id (per_id asc),
    CONSTRAINT fk_bar_attorney 
    FOREIGN KEY (per_id)
    REFERENCES person(per_id)
    ON DELETE NO ACTION 
    ON UPDATE CASCADE
)

ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

/*FOR CREATING SPECIALTY TABLE*/
DROP TABLE IF EXISTS specialty;
CREATE TABLE IF NOT EXISTS specialty (
    spc_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    spc_type VARCHAR(45) NOT NULL,
    spc_notes VARCHAR(255) NULL,
    PRIMARY KEY (spc_id),
    INDEX idx_per_id (per_id asc),

    CONSTRAINT fk_specialty_attorney
    FOREIGN KEY (per_id) REFERENCES attorney(per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)

ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

/*FOR CREATING CASE*/
DROP TABLE IF EXISTS `case`;
CREATE TABLE `case` (
    cse_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    cse_type VARCHAR(45) NOT NULL,
    cse_description TEXT NOT NULL,
    cse_start_date DATE NOT NULL,
    cse_end_date DATE NULL,
    cse_notes VARCHAR(255) NULL,
    PRIMARY KEY (cse_id),
    FOREIGN KEY (per_id)
    REFERENCES judge(per_id)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

/*FOR CREATING ASSIGNMENT TABLE*/
DROP TABLE IF EXISTS assignment;
CREATE TABLE assignment (
    asn_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_cid SMALLINT UNSIGNED NOT NULL,
    per_aid SMALLINT UNSIGNED NOT NULL,
    cse_id SMALLINT UNSIGNED NOT NULL,
    asn_notes VARCHAR(255) NULL,
PRIMARY KEY (asn_id),

INDEX idx_per_cid (per_cid ASC),
INDEX idx_per_aid (per_aid ASC),
INDEX idx_cse_id (cse_id ASC),
UNIQUE INDEX ux_per_cid_per_aid_cse_id (per_cid ASC, per_aid ASC, cse_id ASC),

CONSTRAINT fk_assign_case
FOREIGN KEY (cse_id) 
REFERENCES `case` (cse_id) 
ON DELETE NO ACTION 
ON UPDATE CASCADE,

CONSTRAINT fk_assignment_client
FOREIGN KEY (per_cid)
REFERENCES client (per_id)
ON DELETE NO ACTION
ON UPDATE CASCADE,

CONSTRAINT fk_assignment_attorney
FOREIGN KEY (per_aid)
REFERENCES attorney (per_id)
ON DELETE NO ACTION
ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

/*FOR CREATING PHONE TABLE*/
DROP TABLE IF EXISTS phone;
CREATE TABLE phone (
    phn_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    phn_num BIGINT NOT NULL,
    phn_type ENUM('mobile','home','work','fax') NOT NULL,
    phn_notes VARCHAR(255) NULL,
    PRIMARY KEY(phn_id),
    INDEX idx_per_id (per_id asc),
    CONSTRAINT fk_phone_person
    FOREIGN KEY(per_id) REFERENCES person(per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)

ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

/*DATA FOR PERSON*/
START TRANSACTION;
INSERT INTO person
(per_id, per_ssn, per_delta, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES
(NULL, NULL, NULL, 'Steve','Rogers', '437 Southern Drive', 'Rochester', 'NY', 32440222,'srogers@comcast.net','1923-10-03','c',NULL),
(NULL, NULL, NULL, 'Bruce','Wayne','1007 Mountain Drive','Gotham', 'NY', 003208440,'bwayne@knology.net','1968-03-20','c',NULL),
(NULL, NULL, NULL, 'Peter','Parker','20 Ingram Street','New York','NY',102862341,'pparker@msn.com','1988-09-12','c',NULL),
(NULL, NULL, NULL, 'Jane','Thompson','13563 Ocean View Drive','Seattle','WA',032084409,'jthompson@gmail.com','1978-05-08','c',NULL),
(NULL, NULL, NULL, 'Debra', 'Steele','543 Oak Lane','Milwaukee','WI',286234178,'dsteele@verizon.net','1994-07-19','c',NULL),
(NULL, NULL, NULL, 'Tony','Stark','332 Palm Avenue','Malibu','CA',902638332,'tstark@yahoo.com','1972-05-04','a',NULL),
(NULL, NULL, NULL, 'Hank','Pym','2355 Brown Street','Cleveland','OH',022348890,'hpym@aol.com','1980-08-28','a',NULL),
(NULL, NULL, NULL, 'Bob', 'Best','4902 Avendale Avenue','Scottsdale','AZ',872638332,'bbest@yahoo.com','1992-02-10','a',NULL),
(NULL, NULL, NULL, 'Sandra','Dole','87912 Lawrence Avenue','Atlanta','GA',002348890,'sdole@gmail.com','1990-01-26','a',NULL),
(NULL, NULL, NULL, 'Ben','Avery','6432 Thunderbird Ln','Sioux Falls','SD',562638332,'bavery@hotmail.com','1983-12-24','a',NULL),
(NULL, NULL, NULL, 'Arthur','Curry','3304 Euclid Avenue','Miami','FL',000219932,'acurry@gmail.com','1975-12-15','j',NULL),
(NULL, NULL, NULL, 'Diana','Prince','944 Green Street','Las Vegas', 'NV',332048823,'dprince@sympatico.com','1980-08-22','j',NULL),
(NULL, NULL, NULL, 'Adam','Jurris','98435 Valencia Dr.','Gulf Shores','AL',870219932,'ajurris@gmx.com','1995-01-31','j',NULL),
(NULL, NULL, NULL, 'Judy','Sleen','56343 Rover Ct.','Billings','MT',672048823,'jsleen@sympatico.com','1970-03-22','j',NULL),
(NULL, NULL, NULL, 'Bill','Neiderheim','43567 Netherland Blvd','South Bend', 'IN',320219932,'bneiderheim@comcast.net','1982-03-13','j',NULL);
COMMIT;

/*DATA FOR PHONE*/
START TRANSACTION;
INSERT INTO phone (phn_id, per_id,phn_num,phn_type,phn_notes)
VALUES 
(NULL, 1, 985756278,'fax', 'home fax machine'),
(NULL, 2, 879325487,'mobile', 'do not call after 11pm'),
(NULL, 3, 328647593,'home', NULL),
(NULL, 4, 257816895,'work', 'phone number for secretary'),
(NULL, 5, 175482657,'home', NULL),
(NULL, 6, 487569321,'mobile', 'do not call before 10am'),
(NULL, 7, 684515845,'work', 'work fax machine'),
(NULL, 8, 213448513,'work', 'prefers calls on mobile'),
(NULL, 9, 987542845,'work', NULL),
(NULL, 10, 154686525,'mobile', 'prefers calls during lunch hour'),
(NULL, 11, 984561346,'fax', 'work fax machine for legal docs'),
(NULL, 12, 878158655,'home', 'do not call after 10pm'),
(NULL, 13, 989563182,'home', 'Only use in case of emergency'),
(NULL, 14, 846511582,'home', 'do not call after 9pm'),
(NULL, 15, 894651885,'mobile', 'Call after work');
COMMIT;
/*DATA FOR CLIENT*/
START TRANSACTION;
INSERT INTO client
(per_id,cli_notes)
VALUES
(1, NULL),
(2,NULL),
(3,NULL),
(4,NULL),
(5,NULL);
COMMIT;
/*DATA FOR ATTORNEY*/
START TRANSACTION;
INSERT INTO attorney (per_id, aty_start_date,aty_end_date,aty_hourly_rate,aty_years_in_practice,aty_notes)
VALUES
(6,'2006-06-12',NULL,85,5,NULL),
(7,'2003-08-20',NULL,130,28,NULL),
(8,'2009-12-12',NULL,70,17,NULL),
(9,'2008-06-08',NULL,78,13,NULL),
(10,'2011-09-12',NULL,60,24,NULL);
COMMIT;
/*DATA FOR BAR*/
START TRANSACTION;
INSERT INTO bar
(bar_id,per_id,bar_name,bar_notes)
VALUES
(null,6,'Florida bar',NULL),
(null,7,'Ocala bar',NULL),
(null,9,'New York bar',NULL),
(null,8,'Tallahassee bar',NULL),
(null,10,'North Florida bar',NULL),
(null,6,'Florida bar',NULL),
(null,8,'Ocala bar',NULL),
(null,10,'New York bar',NULL),
(null,8,'Tallahassee bar',NULL),
(null,9,'North Florida bar',NULL),
(null,7,'Florida bar',NULL),
(null,9,'Ocala bar',NULL),
(null,6,'London bar',NULL),
(null,6,'Tallahassee bar',NULL),
(null,7,'Arizona bar',NULL);
COMMIT;
/*DATA FOR SPECIALTY*/
START TRANSACTION;
INSERT INTO specialty (spc_id, per_id,spc_type,spc_notes)
VALUES
(NULL, 6, 'Family Law', NULL),
(NULL, 10, 'Civil Law', NULL),
(NULL, 8, 'Criminal Law', NULL),
(NULL, 10, 'Civil Law', NULL),
(NULL, 9, 'Family Law', NULL);
COMMIT;
/*DATA FOR COURT*/
START TRANSACTION;
INSERT INTO court(crt_id,crt_name,crt_street,crt_city,crt_state,crt_zip,crt_phone,crt_email,crt_url,crt_notes)
VALUES
(NULL,'Leon County Circuit Court','301 South Monroe Street','Tallahassee','FL',323035292,1235489785,'lctc@us.fl.gov','http://www.leoncountycircuitgourt.gov',NULL),
(NULL,'Leon County Traffic Court','1921 Thomasville road','Tallahassee','FL',985476289,215467859,'fsc.us.fl.gov','http://www.flus.org/',NULL),
(NULL,'Florida Supreme Court','500 South Duval Street','Tallahassee','FL',984526869,185468526,'lctc@us.fl.gov','http://www.floridasupremecourt.org/',NULL),
(NULL,'Orange County Courthouse','424 North Orange Avenue','Orange County','FL',323035292,8504880125,'fsc@us.fl.gov','http://www.floridasupremecourt.org/',NULL),
(NULL,'Fifth District Court of Appeals','300 South Beach Street','Daytona Beach','FL',321158763,3862258600,'5dca@us.fl.gov','http://www.5dca.org/',NULL);
COMMIT;
/*DATA FOR JUDGE*/
START TRANSACTION;
INSERT INTO judge(per_id,crt_id,jud_salary,jud_years_in_practice,jud_notes)
VALUES
(11,5,150000,10,NULL),
(12,4,185000,3,NULL),
(13,4,135000,2,NULL),
(14,3,170000,6,NULL),
(15,1,120000,1,NULL);
COMMIT;
/*DATA FOR JUDGE HISTORY*/
START TRANSACTION; 
INSERT INTO judge_hist (jhs_id,per_id,jhs_crt_id,jhs_date,jhs_type,jhs_salary,jhs_notes)
VALUES
(null,11,3,'2009-01-16','i',130000,null),
(null,12,2,'2010-05-27','i',140000,null),
(null,13,5,'2000-01-02','i',115000,null),
(null,13,4,'2005-07-05','i',135000,null),
(null,14,4,'2008-12-09','i',155000,null),
(null,15,1,'2011-07-17','i',120000,'freshman justice'),
(null,11,5,'2010-07-05','i',150000,'assigned to another court'),
(null,12,4,'2012-10-08','i',165000,'became chief justice'),
(null,14,3,'2009-04-19','i',170000,'reassigned to court based upon local area population growth');
COMMIT;
/*DATA FOR CASE*/
START TRANSACTION;
INSERT INTO `case`
(cse_id, per_id, cse_type, cse_description, cse_start_date, cse_end_date, cse_notes)
VALUES
(NULL,13,'Civil Law','Client says that his logo is being used without his consent to promote a rival business','2010-09-09', NULL, 'COPYRIGHT INFRINGMENT'),
(NULL,12,'Criminal Law','client is charged with assaulting her husband during an argument','2009-11-18','2010-12-23','ASSAULT'),
(NULL,14,'Civil Law','Client broke an ankle while shoppign at a local grocery store, no wet floor sign although floor had just been mopped','2008-05-06','2008-07-23','SLIP AND FALL'),
(NULL,11,'Criminal Law','Client was charged with stealing several televisions from his former workplace; client has a solid alibi','2011-06-05',NULL,'GRAND LARCENY'),
(NULL,13,'Criminal Law','charged with posession of 10 grams of cocaine','2011-06-05',NULL,'POSESSION OF AN ILLEGAL SUBSTANCE'),
(NULL,14,'Civil Law','Client alleges newspaper printed false information about his personal activities while he ran a large laundry business','2007-01-19','2007-05-20','DEFAMATION/LIBEL'),
(NULL,12,'Criminal Law','Client charged with murder of his co-worker over a lovers fued; no alibi','2010-01-26','2013-02-28','MURDER'),
(NULL,15,'Family Law','Client alleges that partner is not paying fair child support for her income level; seeks adjustment','2019-03-18',NULL,'CHILD SUPPORT');
COMMIT;
/*DATA FOR ASSIGNMENT*/
START TRANSACTION;
INSERT INTO assignment
(asn_id,per_cid,per_aid,cse_id,asn_notes)
VALUES
(NULL,1,6,7,NULL),
(NULL,2,6,6,NULL),
(NULL,3,7,2,NULL),
(NULL,4,8,2,NULL),
(NULL,5,9,5,NULL),
(NULL,1,10,1,NULL),
(NULL,2,6,3,NULL),
(NULL,3,7,8,NULL),
(NULL,4,8,8,NULL),
(NULL,5,9,8,NULL),
(NULL,4,10,4,NULL);
COMMIT;
DROP PROCEDURE IF EXISTS CreatePersonSSN;
DELIMITER $$
CREATE PROCEDURE CreatePersonSSN()
BEGIN
DECLARE x,y INT;
SET x=1;
WHILE x<=y DO 
SET @delta=RAMDOM_BYTES(64);
SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111;
SET@dssn=unhex(sha2(concat(@delta, @ran_num), 512));
UPDATE person
SET per_ssn= @ssn, per_delta=@delta
WHERE per_id=x;

SET x=x+1;
END WHILE;

END$$
DELIMITER $$;
CALL CreatePersonSSN();
