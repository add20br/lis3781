> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 - Advanced Database Management

## Andre Davis
### Assignment #3 Requirements:

*Sub-Heading:*


1. Screenshot of *your* ERD
2. Optional: SQL code for the required reports
3. Bitbucket repo links: *Your* lis3781 Bitbucket Repo link

#### README.md file should include the following items:

* ![P1 ERD Screenshot](img/erd.png)
* ![P1 Optional Code](img/optionalcode.png)
* Bitbucket Repo Link https://bitbucket.org/add20br/lis3781/


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git-config - get and set global options
2. git-status - show working tree status
3. git-push - Update remote refs along with associated objects
4. git-pull - fetch from and integrate with another repository 
5. git-add - add file contents to the index
6. git-commit - saves changes to the repository
7. git-init - creates an empty repository or reinitializes an existing one

#### Assignment Screenshots:

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](hhttps://bitbucket.org/add20br/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
