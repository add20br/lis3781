USE add20br;
GO

-- ------------------------------------------
-- Creating Person table
-- -------------------------------------------
IF OBJECT_ID (N'dbo.person',N'U') IS NOT NULL
DROP TABLE dbo.person;
GO

CREATE TABLE dbo.person (
    per_id SMALLINT NOT NULL identity(1,1),
    per_ssn BINARY(64) NULL,
    per_delta BINARY(64) NULL,
    per_fname VARCHAR(15) NOT NULL,
    per_lname VARCHAR(30) NOT NULL,
    per_gender CHAR(1) NOT NULL CHECK (per_gender IN('m','f')),
    per_dob DATE NOT NULL,
    per_street VARCHAR(30) NOT NULL,
    per_city VARCHAR(30) NOT NULL,
    per_state CHAR(2) NOT NULL DEFAULT 'FL',
    per_zip INT NOT NULL CHECK (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    per_email VARCHAR(100) NULL,
    per_type CHAR(1) NOT NULL CHECK (per_type IN('c','s')),
    per_notes VARCHAR(45) NULL,
    PRIMARY KEY (per_id),
    CONSTRAINT ux_per_ssn UNIQUE nonclustered (per_ssn ASC)
);

-- ------------------------------------------
-- Creating phone table
-- -------------------------------------------
IF OBJECT_ID (N'dbo.phone',N'U') IS NOT NULL
DROP TABLE dbo.phone;
GO

CREATE TABLE dbo.phone (
    phn_id SMALLINT NOT NULL IDENTITY(1,1),
    per_id SMALLINT NOT NULL, 
    phn_num BIGINT NOT NULL CHECK (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    phn_type CHAR(1) NOT NULL CHECK (phn_type IN('w','h','f','c')),
    phn_notes VARCHAR(255) NULL,
    PRIMARY KEY (phn_id),
    CONSTRAINT fk_phone_person
    FOREIGN KEY (per_id) 
    REFERENCES dbo.person(per_id)

    ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- ------------------------------------------
-- Creating customer table
-- -------------------------------------------
IF OBJECT_ID (N'dbo.customer',N'U') IS NOT NULL
DROP TABLE dbo.customer;
GO

CREATE TABLE dbo.customer (
    per_id SMALLINT NOT NULL,
    cus_balance DECIMAL(7,2) NOT NULL CHECK (cus_balance >=0),
    cus_total_sales DECIMAL(7,2) NOT NULL CHECK (cus_total_sales >=0),
    cus_notes VARCHAR(45) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT fk_customer_person
    FOREIGN KEY (per_id)
    REFERENCES dbo.person (per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE  
);

-- ------------------------------------------
-- Creating SLSREP table
-- -------------------------------------------
IF OBJECT_ID (N'dbo.slsrep',N'U') IS NOT NULL
DROP TABLE dbo.slsrep;
GO

CREATE TABLE dbo.slsrep (
    per_id SMALLINT NOT NULL,
    srp_yr_sales_goal DECIMAL(8,2) NOT NULL CHECK (srp_yr_sales_goal >=0),
    srp_ytd_sales DECIMAL(8,2) NOT NULL CHECK (srp_ytd_sales >=0),
    srp_ytd_comm DECIMAL(7,2) NOT NULL CHECK (srp_ytd_comm >=0),
    srp_notes VARCHAR(45) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT fk_slsrep_person
    FOREIGN KEY(per_id)
    REFERENCES dbo.person(per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);


-- ------------------------------------------
-- Creating sales rep history table
-- -------------------------------------------
IF OBJECT_ID (N'dbo.srp_hist',N'U') IS NOT NULL
DROP TABLE dbo.srp_hist;
GO

CREATE TABLE dbo.srp_hist (
    sht_id SMALLINT NOT NULL IDENTITY(1,1),
    per_id SMALLINT NOT NULL,
    sht_type CHAR(1) NOT NULL CHECK (sht_type IN('i','u','d')),
    sht_modified DATETIME NOT NULL,
    sht_modifier VARCHAR(45) NOT NULL DEFAULT system_user,
    sht_date DATE NOT NULL DEFAULT getDate(),
    sht_yr_sales_goal DECIMAL(8,2) NOT NULL CHECK (sht_yr_sales_goal >= 0),
    sht_yr_total_sales DECIMAL(8,2) NOT NULL CHECK (sht_yr_total_sales >= 0),
    sht_yr_total_comm DECIMAL(8,2) NOT NULL CHECK (sht_yr_total_comm >=0),
    sht_notes VARCHAR(45) NULL,
    PRIMARY KEY(sht_id),

    CONSTRAINT fk_srp_hist_slsrep
        FOREIGN KEY (per_id)
        REFERENCES dbo.slsrep(per_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

-- ------------------------------------------
-- Creating CONTACT table
-- -------------------------------------------
IF OBJECT_ID (N'dbo.contact',N'U') IS NOT NULL
DROP TABLE dbo.contact;
GO

CREATE TABLE dbo.contact (
    cnt_id INT NOT NULL IDENTITY(1,1),
    per_cid SMALLINT NOT NULL,
    per_sid SMALLINT NOT NULL,
    cnt_date DATETIME NOT NULL,
    cnt_notes VARCHAR(255) NULL,
    PRiMARY KEY (cnt_id),

    CONSTRAINT fk_contact_customer
    FOREIGN KEY (per_cid)
    REFERENCES dbo.customer(per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_contact_slsrep
    FOREIGN KEY (per_sid)
    REFERENCES dbo.slsrep(per_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

-- ------------------------------------------
-- Creating order table
-- -------------------------------------------
IF OBJECT_ID (N'dbo.[order]',N'U') IS NOT NULL
DROP TABLE dbo.[order];
GO

CREATE TABLE dbo.[order] (
    ord_id INT NOT NULL IDENTITY(1,1),
    cnt_id INT NOT NULL,
    ord_placed_date DATETIME NOT NULL,
    ord_filled_date DATETIME NULL,
    ord_notes VARCHAR(255) NULL,
    PRIMARY KEY (ord_id),

    CONSTRAINT fk_order_contact
        FOREIGN KEY (cnt_id)
        REFERENCES dbo.contact(cnt_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);


-- ------------------------------------------
-- Creating store table
-- -------------------------------------------
IF OBJECT_ID (N'dbo.store',N'U') IS NOT NULL
DROP TABLE dbo.store;
GO

CREATE TABLE dbo.store (
    str_id SMALLINT NOT NULL IDENTITY(1,1),
    str_name VARCHAR(45) NOT NULL,
    str_street VARCHAR(30) NOT NULL,
    str_city VARCHAR(30) NOT NULL,
    str_state CHAR(2) NOT NULL DEFAULT 'FL',
    str_zip INT NOT NULL CHECK (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_phone BIGINT NOT NULL CHECK (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_email VARCHAR(100) NOT NULL, 
    str_url VARCHAR(100) NOT NULL,
    str_notes VARCHAR(255) NULL,
    PRIMARY KEY (str_id)
)

-- ------------------------------------------
-- Creating invoice table
-- -------------------------------------------
IF OBJECT_ID (N'dbo.invoice',N'U') IS NOT NULL
DROP TABLE dbo.invoice;
GO

CREATE TABLE dbo.invoice (
    inv_id INT NOT NULL IDENTITY (1,1),
    ord_id INT NOT NULL,
    str_id SMALLINT NOT NULL,
    inv_date DATETIME NOT NULL,
    inv_total DECIMAL(8,2) NOT NULL CHECK (inv_total >=0),
    inv_paid BIT NOT NULL,
    inv_notes VARCHAR(255) NULL,
    PRIMARY KEY(inv_id),

    CONSTRAINT ux_ord_id UNIQUE nonclustered (ord_id ASC),
    CONSTRAINT fk_invoice_order
        FOREIGN KEY (ord_id)
        REFERENCES dbo.[order](ord_id)
        ON DELETE CASCADE 
        ON UPDATE CASCADE,

    CONSTRAINT fk_invoice_store
        FOREIGN KEY (str_id)
        REFERENCES dbo.store(str_id)
        ON DELETE CASCADE 
        ON UPDATE CASCADE
);

-- ------------------------------------------
-- Creating PAYMENT table
-- -------------------------------------------
IF OBJECT_ID (N'dbo.payment',N'U') IS NOT NULL
DROP TABLE dbo.payment;
GO

CREATE TABLE dbo.payment (
    pay_id INT NOT NULL IDENTITY(1,1),
    inv_id INT NOT NULL,
    pay_date DATETIME NOT NULL,
    pay_amt DECIMAL(7,2) NOT NULL CHECK (pay_amt >= 0),
    pay_notes VARCHAR(255) NULL,
    PRIMARY KEY (pay_id),

    CONSTRAINT fk_payment_invoice
        FOREIGN KEY (inv_id)
        REFERENCES dbo.invoice (inv_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);
-- ------------------------------------------
-- Creating vendor table
-- -------------------------------------------
IF OBJECT_ID (N'dbo.vendor',N'U') IS NOT NULL
DROP TABLE dbo.vendor;
GO

CREATE TABLE dbo.vendor (
    ven_id SMALLINT NOT NULL IDENTITY(1,1),
    ven_name VARCHAR(45) NOT NULL,
    ven_street VARCHAR(30) NOT NULL,
    ven_city VARCHAR(30) NOT NULL,
    ven_state CHAR(2) NOT NULL DEFAULT 'FL',
    ven_zip INT NOT NULL CHECK (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_phone BIGINT NOT NULL CHECK (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_email VARCHAR(100) NULL,
    ven_url VARCHAR(100) NULL,
    ven_notes VARCHAR (255) NULL,
    PRIMARY KEY (ven_id)
);

-- ------------------------------------------
-- Creating product table
-- -------------------------------------------
IF OBJECT_ID (N'dbo.product',N'U') IS NOT NULL
DROP TABLE dbo.product;
GO

CREATE TABLE dbo.product (
    pro_id SMALLINT NOT NULL IDENTITY(1,1),
    ven_id SMALLINT NOT NULL,
    pro_name VARCHAR(30) NOT NULL,
    pro_descript VARCHAR(45) NULL,
    pro_weight FLOAT NOT NULL CHECK (pro_weight >= 0),
    pro_qoh SMALLINT NOT NULL CHECK (pro_qoh >= 0),
    pro_price DECIMAL(7,2) NOT NULL CHECK (pro_price >= 0),
    pro_cost DECIMAL(7,2) NOT NULL CHECK (pro_cost >= 0),
    pro_discount DECIMAL(3,0) NULL,
    pro_notes VARCHAR(255) NULL,
    PRIMARY KEY(pro_id),

    CONSTRAINT fk_product_vendor
        FOREIGN KEY(ven_id)
        REFERENCES dbo.vendor(ven_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

-- ------------------------------------------
-- Creating product history table
-- -------------------------------------------
IF OBJECT_ID (N'dbo.product',N'U') IS NOT NULL
DROP TABLE dbo.product_hist;
GO

CREATE TABLE dbo.product_hist (
    pht_id INT NOT NULL IDENTITY(1,1),
    pro_id SMALLINT NOT NULL,
    pht_date DATETIME NOT NULL,
    pht_cost DECIMAL(7,2) NOT NULL CHECK(pht_cost >= 0),
    pht_price DECIMAL(7,2) NOT NULL CHECK(pht_price >= 0),
    pht_discount DECIMAL(3,0) NULL,
    pht_notes VARCHAR(255) NULL,
    PRIMARY KEY (pht_id),

    CONSTRAINT fk_product_hist_product
    FOREIGN KEY (pro_id)
    REFERENCES dbo.product(pro_id)
    ON DELETE CASCADE 
    ON UPDATE CASCADE
);

-- ------------------------------------------
-- Creating ORDER LINE table
-- -------------------------------------------
IF OBJECT_ID (N'dbo.order_line',N'U') IS NOT NULL
DROP TABLE dbo.order_line;
GO

CREATE TABLE dbo.order_line (
    oln_id INT NOT NULL IDENTITY(1,1),
    ord_id INT NOT NULL,
    pro_id SMALLINT NOT NULL,
    oln_qty SMALLINT NOT NULL CHECK (oln_qty >= 0),
    oln_price DECIMAL(7,2) NOT NULL CHECK(oln_price >= 0),
    oln_notes VARCHAR(255) NULL,
    PRIMARY KEY (oln_id),

    CONSTRAINT fk_order_line_product
        FOREIGN KEY (pro_id)
        REFERENCES dbo.product(pro_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,

	 CONSTRAINT fk_order_line_order
        FOREIGN KEY (ord_id)
        REFERENCES dbo.[order](ord_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

-- --------------------------------------------
-- Populating Person with data
-- --------------------------------------------
INSERT INTO dbo.person
(per_ssn, per_delta, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
VALUES
(1, NULL, 'Steve','Rogers','m','1923-10-03', '437 Southern Drive', 'Rochester', 'NY', 324402221,'srogers@comcast.net','s',NULL),
(2, NULL, 'Bruce','Wayne','m','1968-03-20','1007 Mountain Drive','Gotham', 'NY', 232084405,'bwayne@knology.net','s',NULL),
(3, NULL, 'Peter','Parker','m','1988-10-03','20 Ingram Street','New York','NY',328623415,'pparker@msn.com','s',NULL),
(4, NULL, 'Jane','Thompson','m','1978-11-22','13563 Ocean View Drive','Seattle','WA',132084405,'jthompson@gmail.com','s',NULL),
(5, NULL, 'Debra', 'Steele','f','1999-03-23','543 Oak Lane','Milwaukee','WI',286234178,'dsteele@verizon.net','s',NULL),
(6, NULL, 'Tony','Stark','m','1959-11-20','332 Palm Avenue','Malibu','CA',902638332,'tstark@yahoo.com','c',NULL),
(7, NULL, 'Hank','Pym','m','1989-02-03','2355 Brown Street','Cleveland','OH',522348890,'hpym@aol.com','c',NULL),
(8, NULL, 'Bob', 'Best','m','1957-08-06','4902 Avendale Avenue','Scottsdale','AZ',872638332,'bbest@yahoo.com','c',NULL),
(9, NULL, 'Sandra','Dole','f','1914-10-19','87912 Lawrence Avenue','Atlanta','GA',102348890,'sdole@gmail.com','c',NULL),
(10, NULL, 'Ben','Avery','m','1998-04-22','6432 Thunderbird Ln','Sioux Falls','SD',562638332,'bavery@hotmail.com','c',NULL),
(11, NULL, 'Arthur','Curry','m','1998-06-07','3304 Euclid Avenue','Miami','FL',100219932,'acurry@gmail.com','c',NULL),
(12, NULL, 'Diana','Prince','f','2000-06-04','944 Green Street','Las Vegas', 'NV',332048823,'dprince@sympatico.com','c',NULL),
(13, NULL, 'Adam','Jurris','m','1966-12-03','98435 Valencia Dr.','Gulf Shores','AL',870219932,'ajurris@gmx.com','c',NULL),
(14, NULL, 'Judy','Sleen','f','1923-10-03','56343 Rover Ct.','Billings','MT',672048823,'jsleen@sympatico.com','c',NULL),
(15, NULL, 'Bill','Neiderheim','m','1923-10-03','43567 Netherland Blvd','South Bend', 'IN',320219932,'bneiderheim@comcast.net','c',NULL);

-- --------------------------------------------
-- Populating sales rep with data
-- --------------------------------------------
INSERT INTO dbo.slsrep
(per_id,srp_yr_sales_goal,srp_ytd_sales,srp_ytd_comm,srp_notes)
VALUES
(1,100000,60000,1800,NULL),
(2,80000,35000,3500,NULL),
(3,150000,84000,9650,NULL),
(4,125000,87000,15300,NULL),
(5,98000,43000,8750,NULL);

-- --------------------------------------------
-- Populating customer with data
-- --------------------------------------------
INSERT INTO dbo.customer
(per_id,cus_balance,cus_total_sales,cus_notes)
VALUES
(6, 120, 14789,NULL),
(7, 98.46, 234.92,NULL),
(8, 0, 4578,'Customer always pays on time.'),
(9, 981.73, 1672.38, 'High balance.'),
(10, 541.23, 782.57, NULL),
(11, 251.02, 13782.90, 'Good customer.'),
(12, 582.67,963.12,'Previously paid in full'),
(13, 121.67, 1057.45, 'Recent customer'),
(14, 765.43, 6789.42, 'Buys bulk quantities'),
(15, 304.39, 456.81, 'Has not purchased recently');

-- --------------------------------------------
-- Populating contact with data
-- --------------------------------------------
INSERT INTO dbo.contact
(per_sid,per_cid,cnt_date,cnt_notes)
VALUES
(1, 6, '1999-01-01',NULL),
(2, 6, '2001-09-29',NULL),
(3, 7, '2002-08-15',null),
(2, 7, '2003-08-15',NULL),
(4, 7, '2002-09-01',NULL),
(5, 8, '2004-02-28',null),
(4, 8, '2004-03-03',null),
(1, 9, '2004-04-07',null),
(5, 9, '2004-07-29',null),
(3, 11, '2005-05-02',NULL),
(4, 13, '2005-06-14',null),
(2, 15, '2005-07-02',null);

-- --------------------------------------------
-- Populating order with data
-- --------------------------------------------
INSERT INTO dbo.[order]
(cnt_id,ord_placed_date,ord_filled_date,ord_notes)
VALUES
(2, '2010-11-23','2010-12-24',NULL),
(3, '2005-03-19','2005-07-28',NULL),
(4, '2011-07-01','2011-07-06',NULL),
(5, '2009-12-24','2010-01-05',NULL),
(6, '2008-09-21','2008-11-26',NULL),
(7, '2009-04-17','2009-04-30',NULL),
(8, '2010-05-31','2010-06-02',NULL),
(9, '2007-09-02','2007-09-07',NULL),
(10, '2011-12-08','2011-12-16',NULL),
(11, '2012-02-29','2012-05-02',NULL);

INSERT INTO dbo.store
(str_name,str_street,str_city,str_state,str_zip,str_phone,str_email,str_url,str_notes)
VALUES
('Walgreens','14567 Walnut Ln','Jackson','MI',986547573,3526879512,'info@walgreens.com','wwww.walgreens.com',NULL),
('The HomeDepot','1448 Apalachee Prkwy','Tallahassee','FL',323115486,8547965821,'theorangelife@hdpo.com','www.thehomedopt.com',NULL),
('Dollar General','1128 Hope Ave.','Providence','RI',523556963,8547965821,'dollargen@comcast.com','www.dollargen.com',NULL),
('CVS','1999 Black Acre Rd','Miami Beach','FL',857431685,1254785698,'help@cvs.com','wwww.cvs.com',NULL),
('Lowes','111 Beechnut Lane','Chicago','IL',478549628,1548695874,'help@lowes.com','www.lowes.com',NULL);

-- --------------------------------------------
-- Populating Invoice with data
-- --------------------------------------------
INSERT INTO dbo.invoice
(ord_id,str_id,inv_date,inv_total,inv_paid,inv_notes)
VALUES
(5,12,'2001-05-03',58.32,0,NULL),
(4,12,'2006-11-11',100.59,0,NULL),
(3,13,'2010-09-16',57.34,0,NULL),
(12,12,'2011-01-10',99.32,1,NULL),
(11,13,'2008-06-24',1109.67,1,NULL),
(6,14,'2009-04-20',239.83,0,NULL),
(7,15,'2010-06-05',537.29,0,NULL),
(8,12,'2007-09-09',644.21,1,NULL),
(9,13,'2011-12-17',934.12,1,NULL),
(10,14,'2012-03-18',27.45,0,NULL);

-- --------------------------------------------
-- Populating vendor with data
-- --------------------------------------------
INSERT INTO dbo.vendor
(ven_name,ven_street,ven_city,ven_state,ven_zip,ven_phone,ven_email,ven_url,ven_notes)
VALUES
('Sysco','531 Dolphin Rd','Orlando','FL','895479625','5984623684','sales@sysco.com','sysco.com',NULL),
('General Electric','227 Giraffe Blvd','Miami Beach','FL','184689254','1548798546','sales@ge.com','genel.com','Good Turnaround'),
('IBM','1911 Lion Prkwy','Hollywood','FL','156849856','5468798542','IBM.help@ibm.com','ibm.com',NULL),
('Goodyear','1242 Hippo Rd','Tallahassee','FL','158978526','8784521456','help@goodyear.com','goodyear.com','Competing well with Firestone.'),
('Microsoft','531 Crocodile Way','Orlando','FL','589624587','1879586254','help@microsoft.com','microsoft.en.com',NULL);

-- --------------------------------------------
-- Populating product with data
-- --------------------------------------------
INSERT INTO dbo.product
(ven_id,pro_name,pro_descript,pro_weight,pro_qoh,pro_cost,pro_price,pro_discount,pro_notes)
VALUES
(1,'hammer','',2.5,45,4.99,7.99,30,'Discounted only when purchased with screwdrivers'),
(2,'screwdriver','',1.8,120,1.99,3.49,NULL,NULL),
(4,'pail','16 Gallon',2.8,48,3.49,7.99,40,NULL),
(5,'cooking oil','Peanut Oil',15,19,19.99,28.99,NULL,'gallons'),
(3,'frying pan','',3.5,178,8.45,13.99,50,'Currently half price sale');

-- --------------------------------------------
-- Populating order_line with data
-- --------------------------------------------
INSERT INTO dbo.order_line
(ord_id,pro_id,oln_qty,oln_price,oln_notes)
VALUES
(3,2,10,8.0,NULL),
(6,3,7,9.88,NULL),
(6,4,3,6.99,null),
(5,1,2,12.76,NULL),
(4,5,13,58.99,NULL);

-- --------------------------------------------
-- Populating payment with data
-- --------------------------------------------
INSERT INTO dbo.payment
(inv_id,pay_date,pay_amt,pay_notes)
VALUES
(23,'2008-07-01',5.99,NULL),
(24,'2010-09-28',4.99,NULL),
(25,'2008-07-23',8.75,NULL),
(26,'2010-10-31',19.55,Null),
(27,'2011-03-29',32.5,NULL),
(26,'2010-04-11',20.51,NULL),
(28,'2009-04-15',105.5,NULL),
(29,'2019-08-22',15.85,NULL),
(30,'2020-09-27',40.00,NULL),
(31,'2021-12-29',1452.5,NULL);

-- --------------------------------------------
-- Populating product hist with data
-- --------------------------------------------
INSERT INTO dbo.product_hist
(pro_id,pht_date,pht_cost,pht_price,pht_discount,pht_notes)
VALUES
(1,'2005-01-02 11:53:34',4.99,7.99,30,'Discounted only when purchased with screwdriver set'),
(2,'2005-02-03 09:13:56',1.99,3.49,NULL,NULL),
(3,'2005-03-02 10:22:11',3.89,7.99,40,NULL),
(4,'2006-05-02 09:54:33',19.99,28.99,30,'gallons'),
(5,'2006-05-02 18:32:31',8.45,13.99,30,'Currently half price sale');

-- --------------------------------------------
-- Populating sales rep hist with data
-- --------------------------------------------
INSERT INTO dbo.srp_hist
(per_id,sht_type,sht_modified,sht_modifier,sht_date,sht_yr_sales_goal,sht_yr_total_sales,sht_yr_total_comm,sht_notes)
VALUES
(1,'u',getDate(),SYSTEM_USER, getDate(), 100000, 110000, 11000, NULL),
(4,'i',getDate(),SYSTEM_USER, getDate(), 150000, 175000, 17500, NULL),
(3,'u',getDate(),SYSTEM_USER, getDate(), 200000, 185000, 18500, NULL),
(2,'u',getDate(),SYSTEM_USER, getDate(), 210000, 220000, 22000, NULL),
(5,'i',getDate(),SYSTEM_USER, getDate(), 225000, 230000, 2300, NULL);

-- --------------------------------------------
-- Populating phone table with data
-- --------------------------------------------
INSERT INTO dbo.phone
(per_id,phn_num,phn_type,phn_notes)
VALUES
(1,8957854261,'w',NULL),
(2,1548657849,'h',NULL),
(3,1254896582,'f',NULL),
(4,1256879851,'c',NULL),
(5,9625486525,'w',NULL),
(6,5489567135,'w',NULL),
(7,2654862546,'f',NULL),
(8,1548978526,'f',NULL),
(9,1457568234,'f',NULL),
(10,2546854713,'c',NULL),
(11,1254625785,'h',NULL),
(12,1238478651,'h',NULL),
(13,3264895782,'w',NULL),
(14,1587985264,'h',NULL),
(15,1547868521,'c',NULL);
GO
-- -----------------------------------------------
-- Procedure for Delta Values
-- -----------------------------------------------
CREATE PROC dbo.CreatePersonSSN
AS
BEGIN

    DECLARE @delta BINARY(64);
    DECLARE @ran_num int;
    DECLARE @ssn BINARY(64)
    DECLARE @x INT, @y INT;
    SET @x = 1;

    SET @y=(select count(*) from dbo.person);

        WHILE (@x <= @y)
        BEGIN
        SET @delta=CRYPT_GEN_RANDOM(63);
        SET @ran_num=floor(rand()*(999999999-111111111+1))+111111111;
        SET @ssn=HASHBYTES('SHA_512', concat(@delta, @ran_num));

        UPDATE dbo.person
        SET per_ssn=@ssn, per_delta=@delta
        WHERE per_id=@x;

        SET @x = @x + 1;

        END;
END;
Go
EXEC dbo.CreatePersonSSN