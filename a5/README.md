> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 - Advanced Database Management

## Andre Davis
### Assignment #5 Requirements:

*Sub-Heading:*


1. Screenshot of *your* ERD
2. Optional: SQL code for the required reports
3. Bitbucket repo links: *Your* lis3781 Bitbucket Repo link

#### Assignment Screenshots:

* ![A4 ERD Screenshot](img/erd_top.png)
* ![A4 ERD Screenshot cont.](img/erd_bottom.png)
* Bitbucket Repo Link: https://bitbucket.org/add20br/lis3781/


