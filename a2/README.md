> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 - Advanced Database Management

## Andre Davis
### Assignment 2

*Sub-Heading:*
#### Assignment Screenshots:

1. ![A2 Screenshot of SQL code](img/sql1.png)
2. ![A2 Screenshot of SQL code](img/sql2.png)
3. ![A2 Screenshot of Populated Tables](img/queryresultset.png)
4. ![A2 Screenshot of User1](img/user1.png)
5. ![A2 Screenshot of User2](img/user2.png)

