0DROP DATABASE IF EXISTS add20br;
CREATE DATABASE IF NOT EXISTS add20br;
USE add20br;

DROP TABLE IF EXISTS add20br.company, add20br.customer;
CREATE TABLE add20br.company
(
cmp_id INT unsigned NOT NULL AUTO_INCREMENT, 
cmp_type ENUM('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership'),
cmp_street VARCHAR(30) NOT NULL,
cmp_city VARCHAR(30) NOT NULL,
cmp_state CHAR(2) NOT NULL,
cmp_zip CHAR(9) NOT NULL,
cmp_phone BIGINT UNSIGNED NOT NULL,
cmp_ytd_sales DECIMAL(10,2) UNSIGNED NOT NULL,
cmp_email VARCHAR(100) NULL,
cmp_url VARCHAR(100) NULL,
cmp_notes VARCHAR(255) NULL,
PRIMARY KEY (cmp_id)
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

INSERT INTO add20br.company
VALUES
(null,'C-Corp','507 - 20th Ave.', 'Jacksonville','FL','081226749','2065559857','12345678.00',null,'https:madeup1.com','company notes1'),
(null,'S-Corp','100 Porters Way', 'Miami Beach','FL','098768547','2982476587','89257615.00',null,'https:madeup2.com','company notes2'),
(null,'LLC','1700 Freedom Boulevard', 'Tallahassee','FL','098246528','9875876801','87935865.00',null,'https:madeup3.com','company notes3'),
(null,'Non-Profit-Corp','1901 Blackstone Road', 'Vero Beach','FL','039857685','8758624568','82546785.00',null,'https:madeup4.com','company notes4'),
(null,'Partnership','1061 Reacher Road', 'Coco','FL','068521456','7548632515','36859785.00',null,'https:madeup5.com','company notes5');
SHOW WARNINGS;

DROP TABLE IF EXISTS add20br.customer;
CREATE TABLE add20br.customer
(
cus_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
cmp_id INT UNSIGNED NOT NULL,
cus_ssn binary(64) NOT NULL,
cus_delta binary(64) NOT NULL,
cus_type ENUM('Loyal','Discount','Impulse','Need-Based','Wandering') NOT NULL,
cus_first VARCHAR(15) NOT NULL,
cus_last VARCHAR(30) NOT NULL,
cus_street VARCHAR(30) NOT NULL,
cus_city VARCHAR(30) NOT NULL,
cus_state CHAR(2) NOT NULL,
cus_zip int(9) UNSIGNED NOT NULL,
cus_phone BIGINT UNSIGNED NOT NULL,
cus_email VARCHAR(100) NULL,
cus_balance DECIMAL(7,2) UNSIGNED NULL,
cus_tot_sales DECIMAL(7,2) UNSIGNED NULL,
cus_notes VARCHAR(255) NULL,
PRIMARY KEY(cus_id),
UNIQUE INDEX ux_cus_ssn(cus_ssn ASC),
INDEX idx_cmp_id(cmp_id ASC),
CONSTRAINT fk_customer_company
FOREIGN KEY (cmp_id)
REFERENCES add20br.company (cmp_id)
ON DELETE NO ACTION
ON UPDATE CASCADE
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;
SHOW WARNINGS;

SET @salt=RANDOM_BYTES(64);

INSERT INTO add20br.customer
VALUES
(null,2,unhex(SHA2(CONCAT(@salt, 000456789),512)), @salt,'Discount','Douglas','McArthur','23 Billings Gate', 'El Paso','TX','085703412','2145559857','test1@mymail.com','8391.87','37642.00','customer notes1'),
(null,2,unhex(SHA2(CONCAT(@salt, 001567899),512)), @salt,'Loyal','John','Kennedy','1400 Woodward Ave', 'El Paso','TX','784528932','5464559857','test2@mymail.com','9402.87','4589.00','customer notes2'),
(null,2,unhex(SHA2(CONCAT(@salt, 001685289),512)), @salt,'Impulse','Dwight','Eisenhower','12 Constitution Way', 'El Paso','TX','085754212','2145896557','test3@mymail.com','10896.87','1493.00','customer notes3'),
(null,2,unhex(SHA2(CONCAT(@salt, 002586726),512)), @salt,'Discount','Winston','Churchill','13 Westminster Abbey', 'El Paso','TX','098513412','2984579857','test4@mymail.com','8535.99','44842.00','customer notes4'),
(null,2,unhex(SHA2(CONCAT(@salt, 007841789),512)), @salt,'Need-Based','Mikhail','Gorbachev','1900 Pennsylvania Avenue', 'El Paso','TX','098563412','3578942857','test5@mymail.com','8745.87','7854.00','customer notes5');

CREATE USER IF NOT EXISTS 'user1'@'localhost' IDENTIFIED BY 'password123';
GRANT select, update, delete on add20br.* to 'user1'@'localhost';

CREATE USER IF NOT EXISTS 'user2'@'localhost' IDENTIFIED BY 'password456';
GRANT select, insert on add20br.customer TO 'user2'@'localhost';

notee;