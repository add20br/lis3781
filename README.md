> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 3781 - Advanced Database Systems

## Andre Davis

### LIS 3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "a1 README.md file")
    * Screenshot of Ampps Installation
    * Git Commands with short descriptions
    * Your ERD Image
    * Bitbucket Repo Links
2. [A2 README.md](a2/README.md "a2 README.md file")
    * Screenshot of SQL code
    * Screenshot of populated tables

3. [A3 README.md](a3/README.md "a3 README.md file")
    * Screenshot of *your* SQL code used to create and populate your tables; 
    * Screenshot of *your* populated tables (w/in the Oracle environment); 
    * Optional: SQL code for a few of the required reports
    * Bitbucket repo links: *Your* lis3781 Bitbucket repo link 

4. [P1 README.md](p1/README.md "p1 README.md file")
    * Screenshot of *your* ERD
    * Optional: SQL code for the required reports
    * Bitbucket repo links: *Your* lis3781 Bitbucket Repo link

5. [A4 README.md](a4/README.md "a4 README.md file")
    * Screenshot of *your* ERD
    * Optional: SQL code for the required reports
    * Bitbucket repo links: *Your* lis3781 Bitbucket Repo link
6. [A5 README.md](a5/README.md "a5 README.md file")
    * Screenshot of *your* ERD
    * Optional: SQL code for the required reports
    * Bitbucket repo links: *Your* lis3781 Bitbucket Repo link
7. [P2 README.md](p2/README.md "p2 README.md file")
    * Screenshot of at least one MongoDB shell command(s), (e.g., show collections)
    * Screenshot of at least *one* required report (i.e., exercise below), and JSON code solution
    * Bitbucket repo links: *Your* lis3781 Bitbucket Repo link
