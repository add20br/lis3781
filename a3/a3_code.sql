DROP TABLE customer CASCADE CONSTRAINTS PURGE;
CREATE TABLE customer
(
cus_id number(3,0) NOT NULL,
cus_fname varchar2(15) NOT NULL,
cus_lname varchar2(30) not null,
cus_street varchar2(30) not null,
cus_city varchar2(30) not null,
cus_state char(2) not null,
cus_zip number(9) not null,
cus_phone number(10) not null,
cus_email varchar2(100),
cus_balance number(7,2),
cus_notes varchar2(255),
CONSTRAINT pk_customer PRIMARY KEY(cus_id) 
);

DROP SEQUENCE seq_cus_id;
CREATE SEQUENCE seq_cus_id
START WITH 1
INCREMENT BY 1
MINVALUE 1
MAXVALUE 10000;

DROP TABLE commodity CASCADE CONSTRAINTS PURGE;
CREATE TABLE commodity
(
com_id number not null,
com_name varchar2(20),
com_price number(8,2) not null,
com_notes varchar2(255),
CONSTRAINT pk_commodity PRIMARY KEY(com_id),
CONSTRAINT uq_com_name UNIQUE(com_name)
);

DROP SEQUENCE seq_ord_id;
CREATE SEQUENCE seq_ord_id
START WITH 1
INCREMENT BY 1
MINVALUE 1
MAXVALUE 10000;

DROP TABLE "order" CASCADE CONSTRAINTS PURGE;
CREATE TABLE "order"
(
ord_id NUMBER(4,0) NOT NULL,
cus_id NUMBER,
com_id NUMBER,
ord_num_units NUMBER(5,0) NOT NULL,
ord_total_cost NUMBER(8,2) NOT NULL,
ord_notes VARCHAR2(255),
CONSTRAINT pk_orders PRIMARY KEY(ord_id),
CONSTRAINT fk_orders_customer
FOREIGN KEY(cus_id)
REFERENCES customer(cus_id),
CONSTRAINT fk_order_commodity
FOREIGN KEY(com_id)
REFERENCES commodity(com_id),
CONSTRAINT check_unit CHECK(ord_num_units >0),
CONSTRAINT check_total CHECK(ord_total_cost >0)
);
--Populating Customer table
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Martha','James','232 Left Ave.','Jefferson','MS',48528,5863248792,'m.james@aol.com',11500.99,'recently moved');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Jimbo','Maxwell','232 Right Ave.','Jackson','MS',87589,3258759624,'coolpants11@fsu.com',2512.85,'needs update');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Martin','Lawrence','West-East Downes St.','Orlando','FL',25486,1758645823,'frozenmilk23@comcast.com',8547.22,'new number');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Michael','Scott','1123 Martin Luther King Ave.','Tampa','FL',39857,1784527895,'scott.mike11@yahoo.com',19587.93,'balance out of date');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'John','Abbott','1061 Patterson Drive.','Providence','RI',24585,3985475862,'abottabot@gmail.com',156.29,'address reported wrong');
COMMIT;
--Sequence is down here because I had a "trial and error" situation.
DROP SEQUENCE seq_com_id;
CREATE SEQUENCE seq_com_id
START WITH 1
INCREMENT BY 1
MINVALUE 1
MAXVALUE 10000;
--Populating commodity
INSERT INTO commodity VALUES (seq_com_id.nextval, 'DVD & Player',109.00,NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Cereal',3.00,'reeses puffs');
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Scabble',29.00,'original');
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Candy',1.89,NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Tums',2.45,'antacid');
COMMIT;
--Populating orders
INSERT INTO "order" VALUES (seq_ord_id.nextval, 1,2,50,200,NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 2,3,30,100,NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3,1,6,654,NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 5,4,24,972,NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3,5,7,300,NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 1,2,5,15,NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 2,3,40,57,NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3,1,4,300,NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 5,4,14,770,NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3,5,15,883,NULL);
COMMIT;

select cus_fname from customer;
select * from commodity;
select * from "order";
