> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 - Advanced Database Management

## Andre Davis
### Project 2 Requirements:

*Sub-Heading:*


1. Screenshot of at least one MongoDB shell command(s), (e.g., show collections)
2. Screenshot of at least *one* required report (i.e., exercise below), and JSON code solution
3. Bitbucket repo links: *Your* lis3781 Bitbucket Repo link

#### README.md file should include the following items:

* ![P2 MongoDB Shell Command](img/mongodb_shell.png)
* ![P2 Required Report 1](img/collections.png)
* ![P2 Required Report 2](img/first_5.png)
* ![P2 Required Report 3](img/number_of_documents.png)
* ![P2 Required Report 4](img/chicken.png)
* Bitbucket Repo Link https://bitbucket.org/add20br/lis3781/


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git-config - get and set global options
2. git-status - show working tree status
3. git-push - Update remote refs along with associated objects
4. git-pull - fetch from and integrate with another repository 
5. git-add - add file contents to the index
6. git-commit - saves changes to the repository
7. git-init - creates an empty repository or reinitializes an existing one
